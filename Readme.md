This source is a *quite* minimal self modifying Common Lisp program.

Its only an starting point to make more advanced versions

To test it:

* (load 'main.lisp')
* (funcall expendable)          ;==> INIT-EXPENDABLE
* (funcall live)
* (funcall expendable)          ;==> (list INIT-KNOWLEDGE (get-decoded-time))

As you can see 'expendable' function changes its definition now printing another information base on some knowledge
learn by the program
